const express = require("express");
const router = express.Router();
const downloadCtrl = require("../controllers/download.ctrl");

router.get("/win/releases", downloadCtrl.get);


module.exports = router;