const express = require("express");
const morgan = require("morgan");
const config = require("./main");
const init = (app) => {
	const root = config.root;
	app.use(morgan('dev'));
	app.use('/dist', express.static( root + "/dist/"));
	app.use((req, res, next) => {
 		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization, X-Access-Token, Origin, Accept');
		next();
	});	
}

module.exports.init = init 