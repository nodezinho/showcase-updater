const config = require("./main");
const init = app => {
	const root = config.root;
	app.use("/dist", require(root + "/routes/download.route"));
	app.use("/hello", require(root + "/routes/hello.route"));
}	
module.exports.init = init;