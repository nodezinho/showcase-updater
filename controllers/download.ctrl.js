const config = require("../config/main.js");
const path = require("path");
const get = (req, res) => {
	res.sendFile(path.join(config.root + "/dist/win/releases"));
}

module.exports.get = get;