const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 9000;

const config = require("./config/main");
config.root = __dirname;
require("./config/express").init(app);
require("./config/routes").init(app);

app.listen(port, () =>{
    console.log(`App listening to port ${port}`);
})
